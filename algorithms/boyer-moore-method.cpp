#include <iostream>
#include <string>
#include <vector>

#define SIZE 256

using namespace std;

void init_shift_table(const string& pattern, vector<short>& shiftTable){
    short pattern_length = pattern.length();

    for (short i = 0; i < 256; i++){
        shiftTable[i] = pattern_length;
    }

    for (short i = 0; i < (pattern_length - 1); i++){
        shiftTable[pattern[i]] = pattern_length - 1 - i;
    }
}

short BM_search(const string& text, const string& pattern, vector<short>& indexes){
    short textLength = text.length();
    short patternLength = pattern.length();

    if (patternLength > textLength) {
        cout << "Ошибка ввода";
        return -1;
    }

    vector<short> shiftTable(256);
    init_shift_table(pattern, shiftTable);

    /*char symbol;
    for (short i = 0; i < 256; i++){
     symbol = i;
     cout << symbol << "\t" << shiftTable[i] << endl;
}*/

    short text_ind = patternLength - 1; // позиция в тексте
    short pattern_ind = patternLength - 1; //позиция в паттерне

    while (text_ind < textLength){
        while (pattern_ind >= 0 && text[text_ind] == pattern[pattern_ind]){
            text_ind--;
            pattern_ind--;
        if (pattern_ind == -1){
            //cout << "new index is: " << text_ind+1 << endl;
            indexes.emplace_back(text_ind);
            }
        }
        text_ind += shiftTable[text[text_ind]]; //сдвиг по таблице
    }
    return -1;
}

int main(){
    string text;
    string pattern;
    vector<short> indexes;

    cout << "Введите текст: ";
    cin >> text;
    cout << "Введите искомый паттерн: ";
    cin >> pattern;

    BM_search(text, pattern, indexes);

    for (short n : indexes){
        cout << n << ", ";
    }

}
