# include <limits.h>
# include <string.h>
# include <stdio.h>

# define SIZE 256

int max(int a, int b){return (a > b) ? a : b;}

void badCharHeuristic(char *str, int size, int badchar[SIZE]){
    int i;
    for (i = 0; i < SIZE; i++)badchar[i] = -1;
    for (i = 0; i < size; i++)badchar[(int) str[i]] = i;
}



void search(char *txt, char *pat){
    int m = strlen(pat);
    int n = strlen(txt);
    int badchar[SIZE];
    badCharHeuristic(pat, m, badchar);
    int shift = 0;

    while (shift <= (n - m)){
        int j = m - 1;
        while (j >= 0 && pat[j] == txt[shift + j])j--;
        if (j < 0){
            printf("%d ", shift);
            shift += (shift + m < n) ? m - badchar[txt[shift + m]] : 1;
        } else shift += max(1, j - badchar[txt[shift + j]]);
    }
}

int main() {
    char text[100], pattern[100];
    printf("Введите строку: ");
    fgets(text, sizeof(text), stdin);
    text[strcspn(text, "\n")] = 0;

    printf("Введите искомый паттерн: ");
    fgets(pattern, sizeof(pattern), stdin);
    pattern[strcspn(pattern, "\n")] = 0;

    printf("Индексы вхождения: ");
    search(text, pattern);

    return 0;
}
