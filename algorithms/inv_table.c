#include <stdio.h>


void next_table(int N, int inv_tab[]){
    for (short i = N-2; i>-1; i--){
        if (inv_tab[i]<(N-1-i)){
            inv_tab[i]+=1;
            break;
        }
        else{
            inv_tab[i]=0;
        }
    }
}


void build_perm(int N, int inv_tab[]){
    int perm[N]; //пустой массив для перестановки
    int used[N]; //пустой массив для слежки за позицией (true/false)

    for (short i = 0; i < N; i++) {
        perm[i] = 0;
        used[i] = 0;
    }

    for (short i = 0; i < N; i++) {
        int skip = inv_tab[i];
        int pos = 0;

        while (skip > 0 || used[pos]) {
            if (!used[pos]) {
                skip--;
            }
            pos++;
        }
        perm[pos] = i + 1;
        used[pos] = 1; // отмечаем занятую позицию
    }

    for (short i = 0; i < N; i++) {
        printf("%d ", inv_tab[i]);
    }

    printf("\t");

    for (short i = 0; i < N; i++) {
        printf("%d ", perm[i]);
    }
    printf("\n");
}

int factorial(int n){
    int factN = 1;
    for (short i = 1; i < (n+1); i++){
        factN *= i;
    }
    return factN;
}

int main() {
    int N;
    printf("Введите количество элементов: ");
    scanf("%d", &N);
    getchar();

    int inv_tab[N];
    for (short i = 0; i<N; i++){
        inv_tab[i] = 0;
    }

    for (short i = 0; i<(factorial(N)-1); i++){
        next_table(N, inv_tab);
        build_perm(N, inv_tab);
        getchar();
    }

    return 0;
}
