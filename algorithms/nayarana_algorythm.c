#include <stdio.h>


//функция перестановки
void rebase(short* a, short* b) {
    short temp = *a;
    *a = *b;
    *b = temp;
}

//ф-я разворота массива
void reverse(short arr[], short start, short end) {
    while (start < end) {
        rebase(&arr[start], &arr[end]);
        start++;
        end--;
    }
}

//генерация следующей перестановки
short next(short arr[], short n) {
    short i = n - 2;

    while (i >= 0 && arr[i] >= arr[i + 1]) {
        i--;
    }

    if (i == -1) {
        return 0;
    }

    short j = n - 1;
    while (arr[j] <= arr[i]) {
        j--;
    }

    rebase(&arr[i], &arr[j]);

    reverse(arr, i + 1, n - 1);
}

//генерация таблицы инверсий
void inversion(short seq[], short n) {
    short inv[n];
    for (short i = 0; i < n; i++) {
        printf("%d ", seq[i]);
    }
    printf("    ");

    for (short i = 0; i < n; i++) {
        inv[i] = 0;
        for (short j = i + 1; j < n; j++) {
            if (seq[i] > seq[j]) {
                inv[i]++;
            }
        }
    }

    for (short i = 0; i < n; i++) {
        printf("%d ", inv[i]);
    }
    printf("\n");
}

int main() {
    short N;
    printf("Введите количество объектов в перестановке: ");
    scanf("%d", &N);
    getchar();

    for (short i = 0; i < N; i++) {
        printf("%d ", i+1);
    }
    printf("    ");
    for (short i = 0; i < N; i++) {
        printf("0 ");
    }
    printf("\n");
    getchar();

    int factN = 1;
    for (short i = 1; i < (N+1); i++){
        factN *= i;
    }

    short seq[N];

    for (short i = 0; i < N; i++) {
        seq[i] = i + 1;
    }

    for (short i = 0; i < (factN-1); i++){
        next(seq, N);
        inversion(seq, N);
        getchar();
    }

    return 0;
}
