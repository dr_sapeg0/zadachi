#include <iostream>

using namespace std;

void Qsort(int arr[], int left_ind, int right_ind){

    if (left_ind>right_ind) return;

    int mid = arr[(left_ind+right_ind)/2];
    int i = left_ind;
    int j = right_ind;

    while (i<=j){
        while (mid > arr[i]) i++;
        while (mid < arr[j]) j--;
        if (i<=j){
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
            i++;
            j--;
        }
    }
    Qsort(arr, left_ind, j);
    Qsort(arr, i, right_ind);

}

void printArray(int arr[], int n){
    for (int i=0; i<n; ++i) cout << arr[i] << " ";
    cout << "\n";
}

int main()
{
    int n;
    cout << "Введите количество элементов: ";
    cin >> n;

    int arr[n] = {};

    srand(time(0));
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 15;
    }

    cout << "Несортированный массив: \n";
    printArray(arr, n);

    Qsort(arr, 0, n-1);

    cout << endl;

    cout << "Сортированный массив: \n";
    printArray(arr, n);
}
