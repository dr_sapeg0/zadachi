#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

void printArray(int arr[], int n){
    for (int i=0; i<n; ++i) cout << arr[i] << " ";
    cout << "\n";
}

void past(int arr[], short N, short number, short step){
    short i = number+step;
    int buffer;
    while (i <= N){
        buffer=arr[i];
        short j = i - step;
        while ((j>=number) && (arr[j] > buffer)){
            arr[j+step] = arr[j];
            j = j - step;
        }
        arr[j+step] = buffer;
        i = i + step;
    }
}

void shell_sort(int arr[], short N){
    short step = 1;
    while(step < N/9){
        step = step*3 + 1;
    }

    while (step >= 1){
        for(short i = 0; i < step; i++){
            past(arr, N, i, step);
            step = (step-1)/3;
        }
    }
}

int main(){

    short N;

    cout << "Введите размер массива: ";
    cin >> N;

    int arr[N] = {};
    srand(time(NULL));
    for (short i = 0; i < N; i++){
        arr[i] = rand() % 100;
    }

    cout << "Несортированный массив: \n";
    printArray(arr, N);

    shell_sort(arr, N);

    cout << "Сортированный массив: \n";
    printArray(arr, N);
}
