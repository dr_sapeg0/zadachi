#include <stdio.h>
#include <string.h>
#include <math.h>

char number[100];
int SoN_1, SoN_2;

/*int pow(int x, int y){
    int result;
    for (short i = 0; i < y; i++){
        result *= x;
    }
    return result;
}*/

int convert_to_dec(const char * number, int SoN) {

        int dec_num = 0;
        int length = strlen(number);

        for (short i = 0; i < length; i++) {
                char c = number[i];
                int value;

                if ((c >= 'A') && (c <= 'F')) {
                        value = c - 65 + 10;
                } else if ((c >=  '0') && (c <= '9')) {
                        value = c - 48;
                } else {
                printf("Недопустимый символ\n");
                return -1;
                }
                dec_num = dec_num * SoN + value;
        }

        return dec_num;
}

float convert_f_to_dec(const char * number, int SoN, int dot_ind){
    char int_part[100], f_part[100];
    int length = strlen(number);
    short ind = 0;

    int int_num;
    float f_num, num;

    for (ind; ind < dot_ind; ind++){
        int_part[ind] = number[ind];
    }

    ind++;
    int_part[dot_ind] = '\0';
    int length_f = 0;

    for (ind, length_f; ind < length; ind++, length_f++){
        f_part[length_f] = number[ind];
    }
    
    length_f++;

    f_part[length_f] = '\0';

    int_num = convert_to_dec(int_part, SoN); 

    for (short i = 0; i < length_f - 1; i++) {
                char c = f_part[i];
                int value;

                if ((c >= 'A') && (c <= 'F')) {
                        value = c - 65 + 10;
                } else if ((c >=  '0') && (c <= '9')) {
                        value = c - 48;
                } else {
                printf("Недопустимый символ\n");
                return -1;
                }
                f_num = f_num * SoN + value;
        }

    f_num = f_num / pow(10, length_f - 1);

    num = int_num + f_num;
    printf("%f", num);
}



void convert_to_other_SoN(int dec, int SoN){
        char symbols[] = "0123456789ABCDEF";
        char new_number[100];
        int ind = 0;

        while (dec > 0) {
                int ost = dec % SoN;
                new_number[ind] = symbols[ost];
                dec /= SoN;
                ind++;
        }

        new_number[ind] = '\0';

        for (short i = 0; i < ind / 2; i++) {
                char temp = new_number[i];
                new_number[i] = new_number[ind - i - 1];
                new_number[ind - i - 1] = temp;
                }

        printf("Новое число: %s \n", new_number);

        }

int main(void) {

    int dot_ind = -1;

    printf("Введите число, которое необходимо перевести и его основание: ");
    scanf("%s %d", &number, &SoN_1);
    printf("Выберите основание системы счисления: ");
    scanf("%d", &SoN_2);

    while((SoN_2 < 2) || (SoN_2 > 16)){
        printf("Недопустимое основание для перевода, повторите попытку ввода: ");
    }

    int length = strlen(number);

    for (short i = 0; i < length; i++){
        if (number[i] == '.'){
            dot_ind = i;
        }
    }

    if ( dot_ind != -1){
        int dec = convert_f_to_dec(number, SoN_1, dot_ind);
        //float fl = convert_f_to_other_SoN(dec, SoN_2);
    } else {
        int dec = convert_to_dec(number, SoN_1);
        convert_to_other_SoN(dec, SoN_2);
    }
}
