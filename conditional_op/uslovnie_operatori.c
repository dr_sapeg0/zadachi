#include <stdio.h>
#include <math.h>

void zd_1(){
    float x, y;
    printf("Введите координаты точки A\n");
    scanf("%f %f", &x, &y);
    printf("%f %f\n", x, y);
    if (((-1<=x) && (x<=0)) && ((0<y) && (y<=1))){
        printf("1. Точка А принадлежит параболе\n");
    }

    else if (((0<x) && (x<=1)) && (y>=x*x)){
        printf("2. Точка А принадлежит параболе\n");
    }

    else{
        printf("Точка А не принадлежит параболе\n");
    }
}

void zd_2(){
    int a, b ,c;
    printf("Введите числа:\n");
    scanf("%d %d %d", &a, &b, &c);
    if ((a<=b) && (b<=c)){
        a = a*a;
        b = b*b;
        c = c*c;
        printf("a=%d, b=%d, c=%d", a, b, c);
    }

    else if ((c<b) && (b<a)){
         b = a;
         c = a;
         printf("a=%d, b=%d, c=%d", a, b, c);
    }
    else{
        a = -1*a;
        b = -1*b;
        c = -1*c;
        printf("a=%d, b=%d, c=%d", a, b, c);
    }

}

void zd_3(){
    char a[5];
    char b[6];
    printf("Введите 4-хзначное число\n");
    scanf("%s", &a);
    printf("Введите 5-тизначное число\n");
    scanf("%s", &b);
    printf("a=%s, b=%s\n", a, b);
    //printf("%c%c%c%c", a[0], a[1], a[2], a[4]);
    if ((a[0]==a[3]) && (a[1] == a[2])){
        printf("%s - это палиндром\n", a);
    }
    else{
        printf("%s - это не палиндром\n", a);
    }

    if ((b[0]==b[4]) && (b[1] == b[3])){
        printf("%s - это палиндром\n", b);
    }
    else{
        printf("%s - это не палиндром\n", b);
    }
}

void zd_4(){
    int a, b, c;
    printf("Введите стороны треугольника:\n");
    scanf("%d %d %d", &a, &b, &c);
    if (((a+b)>c) && ((b+c)>a) && ((a+c)>b)){
        printf("Из этих отрезков можно построить треугольник");
    }
    else{
        printf("Из этих отрезков нельзя построить треугольник");
    }
}

void zd_5(){
    float x, y;
    printf("Введите координаты точки A\n");
    scanf("%f %f", &x, &y);
    if (((0<=y) && (y<=1)) && (x*x + y*y <= 1)){
        printf("Точка А принадлежит окружности\n");
    }
    else{
        printf("Точка А не принадлежит окружности\n");
    }
}

void zd_6(){
    int year;
    printf("Введите год:\n");
    scanf("%d", &year);
    if ((year%4)==0){
        if ((year%100)==0){
            if ((year%400)==0){
                printf("Год високосный\n");
            }
            else{
                printf("Год не високосный\n");
            }
        }
        else{
            printf("Год високосный\n");
        }
    }
    else{
        printf("Год не високосный\n");
    }
}

void zd_7_1(){
    int a, b, c, d;
    printf("Введите положение ладьи:\n");
    scanf("%d %d", &a, &b);
    printf("Введите положение поля:\n");
    scanf("%d %d", &c, &d);
    if ((a==c) || (b==d)){
        printf("Ладья угрожает полю\n");
    }
    else{
        printf("Ладья не угрожает полю\n");
    }
}

void zd_7_2(){
    int x, y;
    printf("Введите положение поля:\n");
    scanf("%d %d", &x, &y);
    if ((y%2)!=0){
        if ((x%2)!=0){
            printf("Поле чёрное\n");
        }
        else{
            printf("Поле белое\n");
        }
    }

    else{
        if ((x%2)!=0){
            printf("Поле белое\n");
        }
        else{
            printf("Поле чёрное\n");
        }
    }
}

int main(){
    zd_7_2();
}
