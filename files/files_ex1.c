#include <stdio.h>

int main(void) {
    FILE * f1 = fopen("f1_ex1.txt", "r");
    FILE * f2 = fopen("f2_ex1.txt", "w");
    char buffer[256];

    if (!f1 || !f2){
        printf("ERROR");
    }
    else{
        while ((fgets(buffer, 256, f1))!=NULL){
            fputs(buffer, f2);
            printf("%s", buffer);
        }
        fclose(f1);
        fclose(f2);
    }
}
