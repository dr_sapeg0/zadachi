#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(void){
    FILE * f1 = fopen("f1_ex2.txt", "rb");
    FILE * f2 = fopen("f2_ex2.txt", "w");
    char buffer[128];
    char *strings[128];
    short count = 0;

    if (!f1 || !f2){
        printf("ERORR");
    }

    while ((fgets(buffer, 128, f1))!=NULL){
        strings[count] = malloc(strlen(buffer)+1);
        strcpy(strings[count], buffer);
        count++;
    }

        for (count = count - 1; count>=0; count--){
        fputs(strings[count], f2);
        printf(strings[count]);
        free(strings[count]);
    }

    fclose(f1);
    fclose(f2);
}
