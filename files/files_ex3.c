#include <string.h>
#include <stdio.h>

int main (void){
    FILE * fp = fopen("f1_ex3.txt", "rb");
    FILE * f_new = fopen("f2_ex3.txt", "w");
    char buffer[256];
    char S1[10] = "mouse";
    char *ptr = NULL;

    if(!fp || !f_new){
        perror("ERROR");
    }

    while ((fgets(buffer, 128, fp))!=NULL){
        ptr = strstr(buffer, S1);
        if (ptr){
            printf("%s", buffer);
            fputs(buffer, f_new);
        }
    }

    fclose(fp);
    fclose(f_new);
}
