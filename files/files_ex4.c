#include <stdio.h>
#include <math.h>

int main(void){
    FILE * f1 = fopen("f1_ex4.txt", "r");
    FILE * f2 = fopen("f2_ex4.txt", "w");
    int age;
    int m[9] = {4, 9, 25, 49, 121, 169, 289, 361};

    if(!f1 || !f2){
        perror("ERROR");
    }

    while(fscanf(f1, "%d", &age) != EOF){
        printf("%d ", age);
        for (short i = 0; i < 9; i++){
            if (age == m[i]){
                fprintf(f2, "%d ", age);
            }
        }
    }

    fclose(f1);
    fclose(f2);
}
