#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void){
    FILE * f1 = fopen("f1_ex5", "rb");
    FILE * f2 = fopen("f2_ex5", "wb");
    
    int t1, t2, t3, x1, x2, y1, y2;
    int count = 0;
    int **massive;

    if(!f1 || !f2){
        perror("ERROR");
    }

    while(fscanf(f1, "%d %d %d %d", &x1, &y1, &x2, &y2) != EOF){
        count++;
    }

    rewind(f1);
    massive = malloc(count * sizeof(int*)); 

    for (short i = 0; i < count; i++){
        massive[i] = malloc(3 * sizeof(int));
        fscanf(f1, "%d %d %d %d", &x1, &y1, &x2, &y2);
        //printf("%d %d %d %d\n", x1, y1, x2, y2);
        massive[i][0] = abs(x2-x1);
        massive[i][1] = abs(y2-y1);
        massive[i][2] = sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
        /*for(short j = 0; j < 3; j++){
            //printf("%d ", massive[i][j]); 
            }
        //printf("\n");*/
    }

    for (short i = 0; i < count; i++){
        for (short j = 0; j < 3; j++){
            if (massive[j][2] > massive[j+1][2]){
                t1 = massive[j][0]; 
                t2 = massive[j][1];
                t3 = massive[j][2];

                massive[j][0] = massive[j+1][0]; 
                massive[j][1] = massive[j+1][1];
                massive[j][2] = massive[j+1][2];

                massive[j+1][0] = t1; 
                massive[j+1][1] = t2;
                massive[j+1][2] = t3;
            }
        }
    }
    
    printf("Минимальную длину содержит отрезок в %d см с координатами {%d;%d}", massive[0][2], massive[0][0], massive[0][1]);
    fprintf(f2, "Длина отрезка: %d\nКоординаты: {%d;%d}\n", massive[0][2], massive[0][0], massive[0][1]);

    for (short i = 0; i < count; i++){
        free(massive[i]);
        /*for (short j = 0; j < 3; j++){
            printf("%d ", massive[i][j]);       //для проверки отсортированного массива
        }*/
    }

    free(massive);
    fclose(f1);
    fclose(f2);
}
