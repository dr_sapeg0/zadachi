#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void){
    FILE * f1 = fopen("f1_ex6", "rb");
    FILE * f2 = fopen("f2_ex6", "wb");
    
    int x, y, R; 
    int count = 0; //количество окружностей = кол-во строк в массиве
    int **massive; // {{координата точки центра по х, к-а по y, радиус окружности}}

    if(!f1 || !f2){
        perror("ERROR");
    }

    while(fscanf(f1, "%d %d %d", &x, &y, &R) != EOF){
        //printf("%d %d %d\n", x, y, R); //выводит считанные параметры
        count++;
    }

    rewind(f1);
    massive = malloc(count * sizeof(int*)); 

    for (short i = 0; i < count; i++){
        massive[i] = malloc(3 * sizeof(int));
        fscanf(f1, "%d %d %d", &x, &y, &R);
        //printf("%d %d %d\n", x, y, R);
        massive[i][0] = x;
        massive[i][1] = y;
        massive[i][2] = R;
        /*for(short j = 0; j < 3; j++){
            printf("%d ", massive[i][j]);       //выводит массив
            }
        printf("\n");*/
    }

    int X1 = massive[0][0]; // Координаты центра
    int Y1 = massive[0][1]; // первой окружности.

    int R1 = massive[0][2]; // Радиус первой окружности.
    
    fprintf(f2, "Параметры первой окружности: центр - {%d;%d}, радиус - %d\n Параметры окружностей с которыми пересекается первая:\n", X1, Y1, R1);

    for (short j = 1; j < count; j++){
        float d = sqrt((X1 - massive[j][0])*(X1 - massive[j][0])+(Y1 - massive[j][1])*(Y1 - massive[j][1]));
        //printf("%f\n", d);             //выводит расстояние между центрами
            if ( d < (R1 + massive[j][2])){
                printf("Первая окружность пересекается с окружностью с центром {%d;%d}. \n", massive[j][0], massive[j][1]);
                fprintf(f2, "\t центр - {%d;%d}, радиус - %d\n", massive[j][0], massive[j][1], massive[j][2]);
            }
    }
    
    for (short i = 0; i < count; i++){
        free(massive[i]);
    }

    free(massive);
    fclose(f1);
    fclose(f2);
}
