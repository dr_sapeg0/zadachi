#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

typedef struct{
    char lastname[30];
    int course;
    char group[10];
} students;

int main(void){

    FILE * f1 = fopen("f1_ex7_en", "rb");
    FILE * f2 = fopen("f2_ex7", "wb");

    students *massive;
    students test;
    int count = 0;

    if(!f1 || !f2){
        perror("ERROR");
    }

    while(fscanf(f1, "%s %d %s", &test.lastname, &test.course, &test.group) != EOF){
        //printf("%s %d %s\n", test.lastname, test.course, test.group); //выводит считанные параметры
        count++;
    }

    rewind(f1);

    massive = malloc(count * sizeof(students));

    printf("\n");
    printf("%-25s | %-10s | %-15s", "      Студент:", "  Курс: ", "   Группа:");  

    for (short i = 0; i < count; i++){
        fscanf(f1, "%s %d %s", &massive[i].lastname, &massive[i].course, &massive[i].group);
    }

    printf("\n");

    for (short i = 0; i < 5; i++) {
        for (short j = 0; j < 4; j++) {
            if (massive[j].lastname[0] > massive[j + 1].lastname[0]) {
                test = massive[j];
                massive[j] = massive[j + 1];
                massive[j + 1] = test;
            }
        }
    }

    fprintf(f2, "%-25s | %-10s | %-15s\n", "      Студент:", "  Курс: ", "   Группа:");  

    for (short i = 0; i < count; i++){
        if (massive[i].course == 1){
        printf("      %-13s|    %-6d|    %s\n", massive[i].lastname, massive[i].course, massive[i].group);
        }
        fprintf(f2, "      %-13s|    %-6d|    %s\n", massive[i].lastname, massive[i].course, massive[i].group);
    }

    free(massive);

    fclose(f1);
    fclose(f2);
}
