#include <iostream>
#include <time.h>

using namespace std;

void init_matrix(int **matrix, int n, int m) {

  for (short i{0}; i < n; i++) {
    matrix[i] = new int[m]{};
  }
}

void check_matrix(int **matrix, int n, int m) {

  if (!matrix) {
    cout << "ERR:memory allocation error.";
  }

  for (short i{0}; i < n; i++) {
    if (!matrix[i]) {

      for (short j{0}; j < m; j++) {
        cout << "ERR:memory allocation error.";
        free(matrix[j]);
        free(matrix);
      }
    }
  }
}

void del_matrix(int **matrix, int n) {

  for (short i{0}; i < n; i++) {
    delete[] matrix[i];
  }

  delete[] matrix;
}

void print_matrix(int **matrix, int row, int column) {

  for (short i{0}; i < row; i++) {
    for (short j{0}; j < column; j++) {
      cout << matrix[i][j] << "  ";
    }
    cout << std::endl;
  }
}

int fill_matrix(int **matrix, int n, int m) {

  for (short i{0}; i < n; i++) {
    for (short j{0}; j < m; j++) {
      matrix[i][j] = rand() % 40 + 30;
    }
  }

  return 0;
}



void bubble(int **arr, int row) {

  int *buffer{new int[2]{0,0}};

  for (short i = 0; i < row; i++) {
    for (short j = 0; j < row - 1; j++) {
      if (arr[j][0] > arr[j+1][0]) {
        buffer[0] = arr[j][0];
        buffer[1] = arr[j][1];

        arr[j][0] = arr[j+1][0];
        arr[j][1] = arr[j+1][1];

        arr[j+1][0] = buffer[0];
        arr[j+1][1] = buffer[1];
      }
    }
  }
}




// row - строка, column - столбец
void sort_matrix(int **matrix, int row, int column) {

  int **new_matrix{new int *[row]{}};
  init_matrix(new_matrix, row, column);

  int **charac{
      new int *[row] {}}; // матрица с харак-й каждого элемента исх. матрицы
  int **charac2{
      new int *[row] {}}; // массив n*2, где первый столбец это харак-ка
  //каждой строки, 2й столбец - порядковый номер в исх. матрице

  init_matrix(charac, row, column);
  init_matrix(charac2, row, 2);

  check_matrix(charac, row, column);
  check_matrix(charac2, row, 2);

  for (short i{0}; i < row; i++) {
    for (short j{0}; j < column; j++) {

      for (short k{1}; k < matrix[i][j]; k++) {
        if ((matrix[i][j] % k) == 0) {
          charac[i][j] += 1;
        }
      }

      if (charac[i][j] == 7) {
        charac2[i][0] += 1;
      }

      charac2[i][1] = i;
    }
  }

  cout << "\n";

  bubble(charac2, row);

  for (short i{}; i < row; i++){
      for(short j{}; j < column; j++){
        new_matrix[i][j] = matrix[charac2[i][1]][j];
    }
  }

  print_matrix(new_matrix, row, column);
  cout << "\n";

  print_matrix(charac2, row, 2);
  cout << "\n";

  del_matrix(charac, row);
  del_matrix(charac2, 2);
}

int main() {

  srand(time(NULL));

  int size;

  cout << "\nВведите размер матрицы: ";
  cin >> size;
  cout << "\n";

  int **matrix{new int *[size] {}};

  init_matrix(matrix, size, size);

  fill_matrix(matrix, size, size);

  cout << "Исходная матрица:\n\n";

  print_matrix(matrix, size, size);

  cout << "\nОтсортированная матрица:\n";

  sort_matrix(matrix, size, size);

  del_matrix(matrix, size);

  cout << "\nprogramm is done!";
}
