#include <stdio.h>

int fact(int N){
    int fact = 1;
    for (short i = 1; i < N; i++){
        fact *= i;
    }
    return fact;
}

int main(){
    short N; //количество объектов

    printf("Введите количество элементов перестановки от 1 до 9: ");
    scanf("%d", &N);

    N++;

    int objects[N];
    int seq_table[N];

    for (short i = 0; i < N; i++){
        objects[i] = i + 1;
        seq_table[i] = 0;
        printf("%d ", objects[N]);
    }

    printf("\n%d", fact(N));
}
