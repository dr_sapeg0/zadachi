#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>
#include <string.h>
#include <algorithm>
#include <string>
#include <vector>
using namespace std;

void ex1(void) {
  char string[1024];
  cout << "enter string> " << endl;
  fgets(string, 1024, stdin);

  short max_len = 0;
  short word_len = 0;
  char max_word[100];
  char *word = strtok(string, ", ");

  while (word != NULL) {
    word_len = strlen(word);
    if (word_len > max_len) {
      max_len = word_len;
      strcpy(max_word, word);
    }
    word = strtok(NULL, " ");
  }
  cout << max_word << endl;
}

void ex3(void) {
  short kolvo_f = 1;
  short count = 0;
  char string[1024];
  cout << "enter string (ex3)> " << endl;
  fgets(string, 1024, stdin);

  char *word = strtok(string, ", ");

  while (word != NULL) {
    for (short i = 0; i < strlen(word); i++) {
      if (word[i] == 'f') {
        count += 1;
      }
    }
    if (count >= kolvo_f) {
      printf("%s   ", word);
    }
    count = 0;
    word = strtok(NULL, " ");
  }
}

void ex2(void) {
  string s;
  string w_to_find = "milk";
  string replace = "million";

  cout << "Введите строку содержащую milk:\n";
  getline(cin, s);

  vector<string> words;
  istringstream iss(s);
  string word;

  while (iss >> word) {
    words.push_back(word);
  }

  for (int i = 0; i < words.size(); i++) {
    if (words[i] == "milk") {
      words[i] = "milion";
    }
    cout << words[i] << " ";
  }
}

void ex4(void) {
    string s;

    cout << "Введите предложение:\n";
    getline(cin, s);

    vector<string> words;
    istringstream iss(s);
    string word;
    vector<string> words2{};
    auto iter{words.begin()};

    while (iss >> word) {
        words.push_back(word);
    }

    for (short i{1}; i < words.size(); i++){
        if ((i % 2) != 0) {
            words2.push_back(words[i]);
        }
    }

    for (short i; i < words2.size(); i++) {
        cout << words2[i] << " ";
    }

    cout  << endl;
}

void ex5(){
    string s;

    cout << "Введите предложение:\n";
    getline(cin, s);

    vector<string> words;
    string word;
    for (char c : s) {
        if (c == ' ') {
            if (!word.empty()) {
                words.push_back(word);
                word.clear();
            }
        }
        else {
            word += c;
        }
    }

    if (!word.empty()) {
        words.push_back(word);
    }

    sort(words.begin(), words.end(), [](const string& a, const string& b) {
        return a.length() < b.length();
    });

    string sortedString;

    for (const string& word : words) {
        if (!sortedString.empty()) {
            sortedString += " ";
        }
        sortedString += word;
    }

    cout << sortedString << endl;
}

string shifr(const string& input){
    string half_word1, half_word2, result;

    for (short i = 0; i < input.length(); i++){
        if (i % 2 == 0){
            half_word1 += input[i];
        } else {
            half_word2 += input[i];
        }
    }

    reverse(half_word2.begin(), half_word2.end());
    result = half_word1 + half_word2;
    return result;
}

void ex6(){
    string s;
    cout << "Введите предложение:\n";
    getline(cin, s);

    vector<string> words;
    string word;

    for (char c : s) {
        if (c == ' ') {
            if (!word.empty()) {
                words.push_back(word);
                word.clear();
            }
        }
        else {
            word += c;
        }
    }

    if (!word.empty()) {
        words.push_back(word);
    }

    /*for (string w : words) {
        cout << w << " ";
    }*/

    int count{};
    string neword1, neword2, neword, newstring;

    for (string w : words){
        for (char c : w){
            count++;
            if (count % 2 == 0){
                neword1 += c;
            }
            else {neword2 += c;}
        }
        reverse(neword2.begin(), neword2.end());
        neword = neword1 + neword2;
        newstring += neword + " ";
        neword1.clear();
        neword2.clear();
        neword.clear();
    }

    cout << "\nЗашифрованная строка: " << newstring << endl;
}

void ex7(){

    string str;
    cout << "Введите предложение:\n";
    getline(cin, str);

    int round_brackets[2] = {0, 0}; //массив {значениие, позиция скобки}
    int sqr_brackets[2] = {0, 0};
    int curly_brackets[2] = {0, 0};
    int count = 0;

    for (char c : str){
        count += 1;
        if (c == '('){
            round_brackets[0] += 1;
            cout << "round +1" << endl;
        }
        if (c == ')'){
            round_brackets[0] -= 1; 
            cout << "round -1" << endl;
            round_brackets[1] = count;
        }
        if (c == '['){
            cout << "sqr + 1" << endl;
            sqr_brackets[0] += 1;
        }
        if (c == ']'){
            cout << "sqr - 1" << endl;
            sqr_brackets[0] -= 1;
            sqr_brackets[1] = count;
        }
        if (c == '{'){
            cout << "curl + 1" << endl;
            curly_brackets[0] += 1;
        }
        if (c == '}'){
            cout << "curl - 1" << endl;
            curly_brackets[0] -= 1;
            curly_brackets[1] = count;
        }
    }

    cout << "\n\n" << count << "\n";
    count = 0;

    cout << round_brackets[0] << "\t" << round_brackets[1] << " () " << endl;
    cout << sqr_brackets[0] << "\t" << sqr_brackets[1] << " [] " << endl;
    cout << curly_brackets[0] << "\t" << curly_brackets[1] << " {} " << endl;

    if ((round_brackets[0] == 0) && (sqr_brackets[0] == 0) && (curly_brackets[0] == 0)){
        cout << 0 << endl;
    }
    else if ((round_brackets[0] > 0) || (sqr_brackets[0] > 0) || (curly_brackets[0] > 0)){
        cout << -1 << endl;
    }
    else if (round_brackets[0] < 0){
        if (round_brackets[0] == -1){
            cout << "Позиция ошибочной скобки " << round_brackets[1] << endl;
        }
        else {
            for(short i = 0; i < round_brackets[1]; i++){
                count++;
                if (str[i] == '('){
                    round_brackets[0] += 1;
                }
                if (str[i] == ')'){
                    round_brackets[0] -= 1; 
                    round_brackets[1] = count;
                }
            }
            cout << "Позиция ошибочной скобки " << round_brackets[1] << endl;
        }
    }
    else if (sqr_brackets[0] < 0){
        if (sqr_brackets[0] == -1){
            cout << "Позиция ошибочной скобки " << sqr_brackets[1] << endl;
        }
        else {
            for(short i = 0; i < sqr_brackets[1]; i++){
                count++;
                if (str[i] == '('){
                    sqr_brackets[0] += 1;
                }
                if (str[i] == ')'){
                    sqr_brackets[0] -= 1; 
                    sqr_brackets[1] = count;
                }
            }
            cout << "Позиция ошибочной скобки " << sqr_brackets[1] << endl;
        }
    }
    else if (curly_brackets[0] < 0){
        if (curly_brackets[0] == -1){
            cout << "Позиция ошибочной скобки " << curly_brackets[1] << endl;
        }
        else {
            for(short i = 0; i < curly_brackets[1]; i++){
                count++;
                if (str[i] == '('){
                    curly_brackets[0] += 1;
                }
                if (str[i] == ')'){
                    curly_brackets[0] -= 1; 
                    curly_brackets[1] = count;
                }
            }
            cout << "Позиция ошибочной скобки " << curly_brackets[1] << endl;
        }
    }


}

int main(void){
    setlocale(LC_ALL, "ru");
    ex7();
}
